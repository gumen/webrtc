class MessagingSample extends HTMLElement {
  set connected(isConnected) {
    this._connected = isConnected

    this._elements.buttonConnect    .disabled =  this._connected
    this._elements.buttonDisconnect .disabled = !this._connected
    this._elements.buttonSendLocal  .disabled = !this._connected
    this._elements.buttonSendRemote .disabled = !this._connected
  }

  set messageLocal(text) {
    this._messageLocal += text.trim() + '\n'
    this._elements.textareaLocalIn.value = this._messageLocal
  }

  set messageRemote(text) {
    this._messageRemote += text.trim() + '\n'
    this._elements.textareaRemoteIn.value = this._messageRemote
  }

  constructor() {
    super()

    this._connected        = false
    this._messageLocal     = ''
    this._messageRemote    = ''
    this._connectionLocal  = null
    this._connectionRemote = null
    this._channelLocal     = null
    this._channelRemote    = null

    this.attachShadow({ mode: 'open' })

    this.shadowRoot.innerHTML = `
      <button id="connect">Connect</button>
      <button id="disconnect">Disconnect</button>

      <h2>Local</h2>

      <section>
        <label for="localOutgoing">Local outgoing messages:</label>
        <textarea
          id="localOutgoing"
          placeholder="Local outgoing messages goes here."></textarea>
        <button id="sendLocal">Send message from local</button>
      </section>

      <section>
        <label for="localIncoming">Local incoming messages:</label>
        <textarea id="localIncoming"
          disabled
          placeholder="Local incoming messages arrive here."></textarea>
      </section>

      <h2>Remote</h2>

      <section>
        <label for="remoteOutgoing">Remote outgoing messages:</label>
        <textarea
          id="remoteOutgoing"
          placeholder="Remote outgoing messages goes here."></textarea>
        <button id="sendRemote">Send message from remote</button>
      </section>

      <section>
        <label for="remoteIncoming">Remote incoming messages:</label>
        <textarea
          id="remoteIncoming"
          disabled
          placeholder="Remote incoming messages arrive here."></textarea>
      </section>
    `

    this._elements = {
      textareaLocalOut  : this.shadowRoot.getElementById('localOutgoing'),
      textareaRemoteOut : this.shadowRoot.getElementById('remoteOutgoing'),
      textareaLocalIn   : this.shadowRoot.getElementById('localIncoming'),
      textareaRemoteIn  : this.shadowRoot.getElementById('remoteIncoming'),
      buttonConnect     : this.shadowRoot.getElementById('connect'),
      buttonDisconnect  : this.shadowRoot.getElementById('disconnect'),
      buttonSendLocal   : this.shadowRoot.getElementById('sendLocal'),
      buttonSendRemote  : this.shadowRoot.getElementById('sendRemote'),
    }

    this._elements.buttonConnect.addEventListener('click', () => {
      this.connect()
    })

    this._elements.buttonDisconnect.addEventListener('click', () => {
      this.disconnect()
    })

    this._elements.buttonSendLocal.addEventListener('click', () => {
      this._sendMessage(this._elements.textareaLocalOut, this._channelLocal)
    })

    this._elements.buttonSendRemote.addEventListener('click', () => {
      this._sendMessage(this._elements.textareaRemoteOut, this._channelRemote)
    })

    // Initial set for "connected" to disable / enable parts of GUI
    this.connected = this._connected
  }

  disconnect() {
    this._connectionLocal.close()
    this._connectionRemote.close()
  }

  async connect() {
    console.log('connect!')

    try {
      const dataChannelParams = { ordered: true }

      this._connectionLocal = new RTCPeerConnection()
      this._connectionLocal.addEventListener('icecandidate', async e => {
        console.log('local connection ICE candidate: ', e.candidate)
        await this._connectionRemote.addIceCandidate(e.candidate)
      })

      this._connectionRemote = new RTCPeerConnection()
      this._connectionRemote.addEventListener('icecandidate', async e => {
        console.log('remote connection ICE candidate: ', e.candidate)
        await this._connectionLocal.addIceCandidate(e.candidate)
      })

      this._channelLocal = this._connectionLocal
        .createDataChannel('messaging-channel', dataChannelParams)
      this._channelLocal.binaryType = 'arraybuffer'

      this._channelLocal.addEventListener('open', (event) => {
        console.log('Local channel open!', event)
        this.connected = true
      })
      this._channelLocal.addEventListener('close', (event) => {
        console.log('Local channel closed!', event)
        this.connected = false
      })
      this._channelLocal.addEventListener('message', e => this._onLocalMessageReceived(e))
      this._connectionRemote.addEventListener('datachannel', e => this._onRemoteDataChannel(e))

      const initLocalOffer = async () => {
        const localOffer = await this._connectionLocal.createOffer()
        console.log(`Got local offer ${JSON.stringify(localOffer)}`)
        return Promise.all([
          this._connectionLocal.setLocalDescription(localOffer),
          this._connectionRemote.setRemoteDescription(localOffer),
        ])
      }

      const initRemoteAnswer = async () => {
        const remoteAnswer = await this._connectionRemote.createAnswer()
        console.log(`Got remote answer ${JSON.stringify(remoteAnswer)}`)
        return Promise.all([
          this._connectionRemote.setLocalDescription(remoteAnswer),
          this._connectionLocal.setRemoteDescription(remoteAnswer),
        ])
      }

      await initLocalOffer()
      await initRemoteAnswer()
    } catch(error) {
      console.error('It\'s a trap!', error)
    }
  }

  _onLocalMessageReceived(event) {
    console.log(`Remote message received by local: ${event.data}`)
    this.messageLocal = event.data
  }

  _onRemoteDataChannel(event) {
    console.log(`onRemote DataChannel: ${JSON.stringify(event)}`)
    this._channelRemote = event.channel
    this._channelRemote.binaryType = 'arraybuffer'
    this._channelRemote.addEventListener('message', e => this._onRemoteMessageReceived(e))
    this._channelRemote.addEventListener('close', () => {
      console.log('Remote channel closed!')
      this.connected = false
    })
  }

  _onRemoteMessageReceived(event) {
    console.log(`Local message received by remote: ${event.data}`)
    this.messageRemote = event.data
  }

  _sendMessage(elementTextarea, channel) {
    if(elementTextarea.value.trim() === '') {
      console.warn('Not sending empty message!')
      return;
    }
    console.log('Sending remote message: ', elementTextarea.value)
    channel.send(elementTextarea.value)
    elementTextarea.value = ''
  }
}

customElements.define('messaging-sample', MessagingSample)
